// On importe directement FunctionComponent via le destructuring ES6
import React, {FunctionComponent, useEffect, useState} from 'react';
import Pokemon from "./models/pokemon";
import PokemonList from "./pages/pokemon-list";
import PokemonsDetail from "./pages/pokemon-detail";
import {BrowserRouter as Router, Switch, Route, Link} from "react-router-dom";
import PageNotFound from "./pages/page-not-found";
import PokemonEdit from "./pages/pokemon-edit";
import PokemonAdd from "./pages/pokemon-add";

/* Typé en FC Function Component - fonction fléchée contenu dans une const
const App: React.FC = () => {
 */
// On utilise directement FunctionComponent - + lisible et sans typage
const App: FunctionComponent = () => {
    // useState('etat initial du composant') -> retourne l'état actuel "name" et la méthode pr changer "setName"
    // <String> type notre state en string
    const [name, setName] = useState<String>('React');
    // Sans destructuring =>
    /*
    * var nameStateVariable = useState('React');
    * var name = nameStateVariable[0];
    * var setName = nameStateVariable[1];
    * */

    const [pokemons, setPokemons] = useState<Pokemon[]>([]); // Initialise avec un array vide

    /* Hook d'effet avec arguments
    1: fonction setPokemon(notre liste)
    2: tableau vide pr ne pas mettre à jour à chaque MaJ
     */
   /* useEffect(() => {
        setPokemons(POKEMONS);
    }, []);
*/

    return (

        <Router>
            {/* met en place le router*/}
            <div>
                {/* barre de nav*/}
                <nav>
                    <div className="nav-wrapper teal">
                        <Link to="/" className={" brand-logo center"}>Pokédex</Link>
                    </div>
                </nav>
                {/* Systeme de gestion des routes - !! Ordre de déclaration importante - 404 en dernier !!*/}
                <Switch>{/* Appel une route à la fois*/}
                    {/* Décrit les routes - 2 props obligatoires chemin path et component associé - exact chemin absolu*/}
                    <Route exact path="/" component={PokemonList} />
                    <Route exact path="/pokemons" component={PokemonList} />
                    <Route exact path="/pokemon/add" component={PokemonAdd} />
                    {/* Route avec param id */}
                    <Route exact path="/pokemons/:id" component={PokemonsDetail} />
                    <Route exact path="/pokemons/edit/:id" component={PokemonEdit}/>
                    {/* Toutes les routes non gérées vont ici!*/}
                    <Route component={PageNotFound}/>
                </Switch>
            </div>
        </Router>
        )
}

//On exporte pour utilisation ailleurs dans l'appli
export default App;

// Version class component
/*
export default class App extends React.Component {
    const name: string = "React";

    render() {
        return <h1>Hell, {name}</h1>;
    }
}
 */