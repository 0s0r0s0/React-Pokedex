import React, {FunctionComponent, useState} from "react";
import PokemonForm from "../components/pokemon-form";
import Pokemon from "../models/pokemon";


const PokemonAdd: FunctionComponent = () => {
    // On génère un id unique avec un Timestamp
    const [id] = useState<number>(new Date().getTime());
    // On crée un new pokemon vierge
    const [pokemon] = useState<Pokemon>(new Pokemon(id));

    {/* On génère notre form */}
    return (
        <div className="row">
            <h2 className="header center">Ajouter un Pokémon</h2>
            <PokemonForm pokemon={pokemon} isEditForm={false}></PokemonForm>
        </div>
    );
}

export default PokemonAdd;