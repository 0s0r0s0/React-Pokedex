// Méthode chargée de formater notre date J/M/A - Méthode qu'on utilise direct ds notre DOM
const formatDate = (date: Date = new Date()): string => {
    // Month + 1 - !! JS renvoie 0 pour janvier
    return `${date.getDate()}/${date.getMonth()+1}/${date.getFullYear()}`;
}

export default formatDate;