import React, {FunctionComponent, useState} from 'react';
import Pokemon from '../models/pokemon';
import './pokemon-card.css';
import formatDate from "../helpers/format-date";
import {useHistory} from 'react-router-dom';
import formatType from "../helpers/format-type";

// on type la Props
type Props = {
    pokemon: Pokemon,
    // Prop facultative avec ?
    borderColor?: string
};

// On passe notre props en param / On met la valeur par défaut de la prop facultative
const PokemonCard: FunctionComponent<Props> = ({pokemon, borderColor = '#009688'}) => {

    const [color, setColor] = useState<string>();
    /*  Récupère l'objet représentant l'historique du nav*/
    const history = useHistory();

    // On prépare les fonctions pour les events onMouseEnter et onMouseLeave
    const showBorder = () => {
        setColor(borderColor);
    }

    const hideBorder = () => {
        setColor('#f5f5f5'); // On remet la bordure en gris
    }
    // Méthode qui prend en param l'id du pokemon - push intègre l'id en param
    const goToPokemon = (id: number) => {
        history.push(`/pokemons/${id}`);
    }

    return (
        <div className="col s6 m4" onClick={ () =>goToPokemon(pokemon.id)} onMouseEnter={showBorder} onMouseLeave={hideBorder}>
            <div className="card horizontal" style={{ borderColor: color}}>
                <div className="card-image">
                    <img src={pokemon.picture} alt={pokemon.name}/>
                </div>
                <div className="card-stacked">
                    <div className="card-content">
                        <p>{pokemon.name}</p>
                        {/* On utilise une méthode JS sur la variable
                         <p><small>{pokemon.created.toString()}</small></p>*/}
                        <p><small>{formatDate(pokemon.created)}</small></p>
                        {pokemon.types.map(type=> (
                           <span key={type} className={formatType(type)}>{type}</span>
                        ) )}
                    </div>
                </div>
            </div>
        </div>
    );
}

export default PokemonCard;