import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';

// Méthode render() + composant App + injection dans le root
ReactDOM.render(
    <App />,
    document.getElementById('root')
);